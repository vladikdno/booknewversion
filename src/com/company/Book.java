package com.company;

import java.util.Arrays;

public class Book {
    public String title;
    public String[] author;
    public int publishYear;
    public String publisher;
    public String author1;


    public Book() {     //создаем класс без каких либо данных чтобы в генераторе не агрилось и не выдавало ошибку через selectNOne
    }

    public Book(String title, String[] author, int publishYear, String publisher) {
        this.title = title;
        this.author = author;
        this.publishYear = publishYear;
        this.publisher = publisher;
    }

    public Book(String title, String author1, int publishYear, String publisher) {
        this.title = title;
        this.author1 = author1;
        this.publishYear = publishYear;
        this.publisher = publisher;
    }

    public String getInfo() {
        return String.format("%s %s %d %s", title, Arrays.toString(author), publishYear, publisher);
    }

    public String[] getAuthor() {
        return author;
    }
}
